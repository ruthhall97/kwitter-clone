import React from "react";
import { LoginFormContainer, MenuContainer } from "../components";

export const HomeScreen = () => (
  <>
    <MenuContainer />
    <h2> Member Login</h2>
    <LoginFormContainer />
  </>
);
