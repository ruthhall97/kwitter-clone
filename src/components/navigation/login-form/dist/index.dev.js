"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "LoginFormContainer", {
  enumerable: true,
  get: function get() {
    return _LoginForm.LoginForm;
  }
});

var _LoginForm = require("./LoginForm");